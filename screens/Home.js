import React from 'react';
import { SafeAreaView,StyleSheet,StatusBar, View,ScrollView } from 'react-native'
import HeaderTabs from '../components/HeaderTabs';
import ImageSlider from '../components/ImageSlider';


import SearchBar from '../components/SearchBar';
import Categoriles from '../components/categoriles';
import SecondSlider from '../components/SecondSlider';
import SecondCatagorils from '../components/SecondCatagorils';
import ShortMessages from '../components/ShortMessages';
import ThirdImagesSlider from '../components/ThirdImagesSlider';
import ForthImageSlider from '../components/ForthImageSlider';



export default function Home(){
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.headertabs}>
                <HeaderTabs />
                <SearchBar />

                <ScrollView showsVerticalScrollIndicator={false}>
                    <ImageSlider />
        
                    <Categoriles />

                    <SecondSlider />

                    <SecondCatagorils />

                    <ShortMessages />

                    <ThirdImagesSlider />

                    <Categoriles />

                    <ForthImageSlider />
                    <SecondCatagorils />
                    

                    

                </ScrollView>



                

                

            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:StatusBar.currentHeight + 10,
        backgroundColor: '#eee',
    },
    headertabs:{
        backgroundColor: 'white',
        padding: 16,
        marginBottom: 100

    }
});
  