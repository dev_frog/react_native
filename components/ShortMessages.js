import React from 'react'
import { View, Text } from 'react-native'

export default function ShortMessages() {
    return (
        <View
            style={{
                backgroundColor: 'green',
                padding: 10,
                alignItems: 'center',
                fontWeight: 'bold',
                fontSize: 18,
                color: '#fff'
            }}
        >
            <Text style={{color: "#fff"}}>Stay Home and Stay Safe</Text>
        </View>
    )
}
