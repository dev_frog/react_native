import React from 'react'
import { View, Text,Image } from 'react-native'

export default function ThirdImagesSlider() {
   
    return (
        <View style={{ marginTop: 10, paddingVertical: 10, paddingLeft: 10}}>
            <Image style={{width: '100%',height: 160,}}  source={{uri: 'http://bsmedia.business-standard.com/_media/bs/img/article/2018-04/03/full/1522703269-5987.jpg'}} />
        </View>
    )
    
}
