import React, {useState, useEffect} from 'react'
import { View, Text,Image, StyleSheet , FlatList, Animated } from 'react-native'


import {images} from '../assets/data'
const height = 220;
const width = '100%';



const ImageSlider = (props) => {
    return (
        <View style={{ marginTop: 10, paddingVertical: 10, paddingLeft: 10}}>
            <Image style={{width: '100%',height: 160,}}  source={{uri: 'https://cdn.statically.io/img/adsofbd.com/f=auto/wp-content/uploads/2019/07/Bashundhara-Fortified-Soybean-Oil-TVC-1.png'}} />
        </View>
    )

return null
}



const styles = StyleSheet.create({
    cardView: {
        flex: 1,
        width: width,
        height: height / 3,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 10,
        shadowColor : '#000',
        shadowOffset: { width: 0.5, height: 0.5},
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
    },
    textView:{
        position: 'absolute',
        bottom: 10,
        margin: 10,
        left: 5
    },
    image: {
        width: width - 20,
        height: height / 3,
        borderRadius: 10,
    },
    itemTitle: {
        color: 'white',
        fontSize: 22,
        shadowColor: '#000',
        shadowOffset:{width: 0.8, height: 0.8},
        shadowOpacity: 1,
        shadowRadius: 3,
        marginBottom: 5,
        fontWeight: 'bold',
        elevation: 5,
    },
    itemDescription: {
        color: 'white',
        fontSize: 12,
        shadowColor: '#000',
        shadowOffset: { width: 0.8, height: 0.8},
        shadowOpacity: 1,
        shadowRadius: 3,
        elevation: 5,
    }

})


export default  ImageSlider