import React, {useState} from 'react';
import { View, Text, TouchableOpacity,StyleSheet } from 'react-native'


export default function HeaderTabs(){

   const [activeTab, setActiveTab] = useState("বাজার");
    return (
        <View style={styles.headeRow}>
            <HeaderButton text="বাজার" btncolor="black" textcolor="white" activeTab={activeTab} setActiveTab={setActiveTab}  />
            <HeaderButton text="দোকান"  btncolor="white" textcolor="black" activeTab={activeTab} setActiveTab={setActiveTab} />
        </View>
    )
}


const HeaderButton = (props) => (
        <TouchableOpacity style={{
            backgroundColor: props.activeTab === props.text ? "black" : "white" ,
            paddingVertical: 7,
            paddingHorizontal: 24,
            borderRadius: 20,
        }}
        onPress={() => props.setActiveTab(props.text)}
        >
            <Text style={{
                color: props.activeTab === props.text ? "white" : "black"
            }}>{props.text}</Text>
        </TouchableOpacity>
        
);


const styles =  StyleSheet.create({
    headeRow: {
        flexDirection: 'row',
        alignSelf: "center",
    },
   
});
  