import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/native-stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'

import IonicIcon from 'react-native-vector-icons/Ionicons'
import {Text, Dimensions } from 'react-native'

// Import Screen

import Home from '../screens/Home'
import Tutorial from '../screens/Tutorial'
import Order from '../screens/Order'
import Khata from '../screens/Khata'

const fullScreenWidth = Dimensions.get('window').width;

const Stack = createStackNavigator()

function HomeStackScreen() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="হোম" component={Home} />
        </Stack.Navigator>
    )
}

function TutorialStackScreen(){
    return (
        <Stack.Navigator>
            <Stack.Screen name="টিউটোরিয়াল" component={Tutorial} />
        </Stack.Navigator>
    )
}

function KhataStackScreen(){
    return (
        <Stack.Navigator>
            <Stack.Screen name="খাতা" component={Tutorial} />
        </Stack.Navigator>
    )
}

function OrderStackScreen(){
    return (
        <Stack.Navigator>
            <Stack.Screen name="আমার অডার" component={Tutorial} />
        </Stack.Navigator>
    )
}


const Tab = createBottomTabNavigator();


export default function Navigation() {
    return (
        <NavigationContainer>
            <Tab.Navigator 
                
            >
                <Tab.Screen name="হোম" component={HomeStackScreen} />
                <Tab.Screen name="টিউটোরিয়াল" component={TutorialStackScreen} />
                <Tab.Screen name="আমার অডার" component={KhataStackScreen} />
                <Tab.Screen name="খাতা" component={OrderStackScreen} />
            </Tab.Navigator>
        </NavigationContainer>
    )
}
