import React from 'react'
import { View, Text,Image } from 'react-native'

export default function ForthImageSlider() {
    return (
        <View style={{ marginTop: 10, paddingVertical: 10, paddingLeft: 10}}>
            <Image style={{width: '100%',height: 160,}}  source={{uri: 'https://bangladeshpost.net/webroot/uploads/featureimage/2020-02/5e42b17ee238f.jpg'}} />
        </View>
    )
}
