import React from 'react'
import { View, Text,ScrollView,Image } from 'react-native'


const items = [
    {
        image: require('../assets/images/Rang-Fashion-House-Bangladesh.jpg'),
        text: 'Rang House',
    },
    {
        image: require('../assets/images/anjans.png'),
        text: 'Anjans'
    },
    
    
];


export default function SecondCatagorils() {
    return (
        <View
        style={{
          marginTop: 2,
          backgroundColor: "#fff",
          paddingVertical: 10,
          paddingLeft: 20,
        }}
      >
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {items.map((item, index) => (
            <View key={index} style={{ alignItems: "center", marginRight: 30 }}>
              <Image
                source={item.image}
                style={{
                  width: 70,
                  height: 60,
                  resizeMode: "contain",
                }}
              />
              <Text style={{ fontSize: 13, fontWeight: "900" }}>{item.text}</Text>
            </View>
          ))}
        </ScrollView>
      </View>
    )
}
